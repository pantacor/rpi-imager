# Build AppImage for x86-64 under Docker

To build, just run the following command:

`docker run --rm $(docker build -q -f Dockerfile.amd64 .) > Imager-v1.4-x86_64.AppImage`

You can then chmod +x the result, and package it in a zip file for distribution. The zip file is
the preferred method as it carries the executable flag upon extraction, removing the need for the
end user to set the executble flag explicitly and thus making it easier for most.
